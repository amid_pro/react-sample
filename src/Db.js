const server = 'http://localhost:8080';


const getData = (method, path, data) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, server + path, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.onload = () => {
          if (xhr.status >= 200 && xhr.status < 300) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject({
              status: xhr.status,
              statusText: xhr.statusText
            });
          }
        };
        xhr.onerror = () => {
          reject({
            status: xhr.status,
            statusText: xhr.statusText
          });
        };
        xhr.send(data);
    });
};


module.exports = {

	getItems: async () => {
        const data = await getData('GET', '/items', null);
        return data;
	},

	addOrder: async (name, email, items) => {
        const order = await getData('POST', '/order', JSON.stringify({ 
            name: name,
            email: email,
            items: items 
        }));
        return order;
    }

};