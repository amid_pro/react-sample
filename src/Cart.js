import React, {Component} from 'react';
import { addOrder } from './Db';
import store from "./store/Store";


export default class Cart extends Component {


	constructor(props){
		super(props);
		this.state = {
			complete: false,
			cart_items: {},
			count: 0,
			showForm: true,
			name: "",
			email: "",
			button_disabled: true
		};
	}


	changeButtonDisabled = () => {

		let disabled = false;

		if (this.state.name.length === 0 || this.state.email.length === 0){
			disabled = true;
		}

		this.setState({
			button_disabled: disabled
		});
	}


	componentDidUpdate(prevProps, prevState){
		if (this.state.name !== prevState.name){
			this.changeButtonDisabled();
		}
		if (this.state.email !== prevState.email){
			this.changeButtonDisabled();
		}
	}


	componentDidMount(){
		store.subscribe(() => {
			this.setState({
				count: store.getState().count,
				cart_items: store.getState().cart_items,
				complete: false
			});
		});
	}


	toggleForm = () => {
		this.setState((prevState) => {
			return {
				showForm: !prevState.showForm
			}
		});
	}


	sendOrder = () => {
		addOrder(this.state.name, this.state.email, this.state.cart_items).then((data) => {
			store.dispatch({
				type: "DOWN_COUNT"
			});
			this.setState({
				complete: true
			});
		});
	}


	render(){

		const form = <form>
  					 	<input type="text" onChange={(e) => this.setState({ name: e.target.value })} className="form-control" placeholder="Имя" />
  					 	<input type="email" onChange={(e) => this.setState({ email: e.target.value })} className="form-control" placeholder="Email" />
  					 	<button disabled={this.state.button_disabled} onClick={() => this.sendOrder()} type="button" className="btn btn-default">Отправить</button>
					  </form>

		const cart_items = Object.keys(this.state.cart_items).map((index) => {
			return <p key={index}>#{index} = {this.state.cart_items[index]}</p>
		});

		const order_complete = this.state.complete ? <p>Заказ оформлен</p> : null;

		return (<div className="navbar content">
					<button onClick={() => this.toggleForm()} className="btn btn-info">Корзина <span className="badge">{this.state.count}</span></button>
					
					{order_complete}

					{(this.state.showForm && this.state.count > 0 && !this.state.complete) ?
						<div className="content">
						{form}
						{cart_items}
						</div>
					: null}

				</div>);
	}

}