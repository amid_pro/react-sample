import React, {Component} from 'react';
import store from "./store/Store";

export default class Item extends Component {


	addToCart = (id) => {
		store.dispatch({
	      type: "ADD_ITEM",
	   	  id: id
	    });
	}

	
	render(){
		return (<div className="col-md-4">
			    	<div className="thumbnail">
			      		<img src={this.props.item.image} />
					      <div className="caption">
					        <h3>{this.props.item.name}</h3>
					        <p>{this.props.item.price}</p>
					        <p><button onClick={() => this.addToCart(this.props.item.id)} className="btn btn-primary">Заказ</button></p>
					      </div>
			    	</div>
			  	</div>);
	}

}
