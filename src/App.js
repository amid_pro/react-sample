import React, {Component} from 'react';
import Loader from './Loader';
import { getItems } from './Db';
import Content from './Content';
import Cart from './Cart';


export default class App extends Component {


    constructor(){
        super();
        this.state = {
            loading: false,
            items: []
        };
    }


    componentDidMount(){
        getItems().then((data) => {
            this.setState({
              items: data,
              loading: true
            });
        });
    }


    render(){

        let content = <Loader />;

        if (this.state.loading){
            content = <React.Fragment>
                      <Cart />
                      <Content items={this.state.items} />
                      </React.Fragment>;
        }

        return content;
    }

}