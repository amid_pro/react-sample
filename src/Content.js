import React, {Component} from 'react';
import Item from './Item';


export default class Content extends Component {


	constructor(props){
		super(props);
		this.state = {
			content: null
		};
	}


	render(){

		const items = this.props.items.map((item) => {
			return <Item key={item.id} item={item} />
		});

		return (<div className="row">{items}</div>);
	}

}