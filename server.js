const express = require('express');
const app = express();
const Sequelize = require('sequelize');


const sequelize = new Sequelize('react', 'root', '', {
  	dialect: 'mysql'
});


const Items = sequelize.define('items', {
	id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false },
  	image: { type: Sequelize.STRING(255), max: 255 },
  	name: { type: Sequelize.STRING(255), max: 255 },
  	price: { type: Sequelize.INTEGER }
}, {timestamps: false, freezeTableName: true, tableName: 'items'});


const Orders = sequelize.define('orders', {
	id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false },
  	name: { type: Sequelize.STRING(255), max: 255 },
  	email: { type: Sequelize.STRING(255), max: 255 }
}, {timestamps: false, freezeTableName: true, tableName: 'orders'});


const OrdersItems = sequelize.define('orders_items', {
	id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false },
  	order_id: { type: Sequelize.INTEGER },
  	item_id: { type: Sequelize.INTEGER },
  	item_count: { type: Sequelize.INTEGER }
}, {timestamps: false, freezeTableName: true, tableName: 'orders_items'});


Orders.hasMany(OrdersItems, { foreignKey: 'order_id', targetKey: 'id', onDelete: 'cascade' });
sequelize.sync({force: true}).then(()=>{
	Items.bulkCreate([

	 	{
	        image: 'http://angular.github.io/angular-phonecat/step-8/app/img/phones/motorola-xoom-with-wi-fi.0.jpg', 
	        name: 'Motorola XOOM with Wi-Fi', 
	        price: 2000
	    }, 
	    {
	        image: 'http://angular.github.io/angular-phonecat/step-8/app/img/phones/motorola-xoom.0.jpg', 
	        name: 'MOTOROLA XOOM', 
	        price: 4000
	    }, 
	    {
	        image: 'http://angular.github.io/angular-phonecat/step-8/app/img/phones/motorola-atrix-4g.0.jpg', 
	        name: 'MOTOROLA ATRIX 4G', 
	        price: 5000
	    }

	]);
});


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.get('/items', (req, res) => {
	Items.findAll({raw:true}).then((data) => {
		res.json(data);
	});
});


app.post('/order', (req, res) => {
	Orders.create({ name: req.body.name, email: req.body.email }).then((order) => {
		
		Object.keys(req.body.items).map((index) => {
			OrdersItems.create({
				order_id: order.id,
			  	item_id: index,
			  	item_count: req.body.items[index]
			});
		});

		return order.id;

	}).then((order_id) => {
		res.json({order_id: order_id});
	});
});


app.use('*', (req, res) => {
  	res.send('Hello World!');
});


app.listen(8080);